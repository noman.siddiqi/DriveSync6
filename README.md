# Introduction
This is the repository for the IMS DriveSync6 Automated API Testing with Postman. It contains DPE and OEM collections with CI/CD yaml file. 

# Documentation
The documentation for API Test Automation is in the [DS6 API Test Automation Guide](https://trakglobalgroup.atlassian.net/wiki/spaces/TCOM/pages/1362297174/DS6+API+Test+Automation+Guide)

# Build and Test
Newman is a command line agent that allows to run multiple requests sequentially. It takes as input the Postman Collection extracted as a json file and return the responses with the results of the each test. CI/CD pipeline run the tests by Newman and publish the test execution results into JIRA. It also generates the Newman html report and send the email with html report if build fails.

# Technologies:
1. Postman
2. nodejs
3. npm
4. Newman
5. docker image



