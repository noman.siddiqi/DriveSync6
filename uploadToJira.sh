stages:
    - test
    - build
    - notify
              
postman_tests:    
    stage: test
    image: 
        name: postman/newman
        entrypoint: [""]
    script:
        - npm install -g newman newman-reporter-htmlextra
        - newman --version
        - npm install -g newman-reporter-html
        - npm install -g newman-reporter-junitfull
        - mkdir reports
        - newman run PayPalRequest.postman_collection.json --reporters cli,htmlextra,junit,junitfull --reporter-htmlextra-export reports/report.html --reporter-junit-export reports/report.xml --reporter-junitfull-export reports/junitfullreport.xml
    artifacts:
        when: always
        paths:
            - reports/
        reports:
            junit: reports/report.xml

importing_test_execution_result:
    stage: build
    script:
        - |
            echo "building amazing repo..."
            export token=$(curl -H "Content-Type: application/json" -X POST --data "{ \"client_id\": \"$client_id\",\"client_secret\": \"$client_secret\" }" https://xray.cloud.xpand-it.com/api/v1/authenticate| tr -d '"')
            echo $token
            curl -H "Content-Type: text/xml" -X POST -H "Authorization: Bearer $token" --data @reports/junitfullreport.xml  "https://xray.cloud.xpand-it.com/api/v2/import/execution/junit?projectKey=DPE&testExecKey=DPE-689"
            echo "done"
    when: always      
                
send_email:
    stage: notify
    when: on_failure
    script: curl -s --user "api:$MAILGUN_API_KEY" 
      "https://api.mailgun.net/v3/$MAILGUN_DOMAIN/messages"
      -F from='Gitlab <gitlab@example.com>'
      -F to=$GITLAB_USER_EMAIL
      -F subject='Test results + report'
      -F text='Testing some Mailgun awesomeness!'
      -F attachment='@reports/report.html'


    #   **************************************************
    stages:
    - test

paypal_test:
    stage: test
    trigger:
        include: config/paypalTest.yml    

collectionTwo_test:
    stage: test
    trigger:
        include: config/CollectionTwo.yml    

