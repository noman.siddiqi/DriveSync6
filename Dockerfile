FROM timbru31/java-node
RUN npm install -g newman-run
RUN npm install -g newman newman-reporter-htmlextra
RUN npm install -g newman-reporter-html
RUN npm install -g newman-reporter-junitfull
RUN whereis java

ENV JAVA_HOME /opt/java/openjdk/
ENV PATH $PATH:/opt/java/openjdk/bin
RUN echo $JAVA_HOME

RUN  npm install -g allure-commandline
RUN  npm install -g newman-reporter-allure
RUN newman --version
RUN allure --version
